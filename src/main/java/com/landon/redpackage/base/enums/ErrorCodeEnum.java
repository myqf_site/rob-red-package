package com.landon.redpackage.base.enums;

/**
 * @author ：shishaopeng
 * @date ：Created in 2021/5/1 11:12 下午
 * @description：状态码枚举类
 */
public enum ErrorCodeEnum {

    /**
     * 成功
     */
    SUCCESS(20000,"success"),
    /**
     * 请求权限不足
     */
    ACCESS_DENIED_EXCEPTION(40001,"access denied exception"),
    /**
     * 其他异常
     */
    OTHER_EXCEPTION(50000,"other exception"),
    /**
     * 业务异常
     */
    SERVICE_EXCEPTION(50001,"service exception");

    private final int value;

    private final String reasonPhrase;

    ErrorCodeEnum(int value, String reasonPhrase) {
        this.value = value;
        this.reasonPhrase = reasonPhrase;
    }

    public int getValue() {
        return this.value;
    }

    public String getReasonPhrase() {
        return this.reasonPhrase;
    }
}
