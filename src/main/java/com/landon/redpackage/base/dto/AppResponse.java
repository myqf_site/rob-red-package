package com.landon.redpackage.base.dto;

import com.landon.redpackage.base.enums.ErrorCodeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author ：shishaopeng
 * @date ：Created in 2021/5/1 11:07 下午
 * @description：请求响应实体
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AppResponse<T> {

    private boolean success;

    private String message;

    private T data;

    private int code;

    public static <T> AppResponse<T> success(T data) {
        return new AppResponse<>(true, "success", data, ErrorCodeEnum.SUCCESS.getValue());
    }

    public static <T> AppResponse<T> failed(String message,int value) {
        return new AppResponse<>(false, message, null,value);
    }

    public static <T> AppResponse<T> failed(String message, T data,int value) {
        return new AppResponse<>(false, message, data,value);
    }
}
