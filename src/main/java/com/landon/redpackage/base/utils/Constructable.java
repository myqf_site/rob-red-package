package com.landon.redpackage.base.utils;

/**
 * @author SSP
 * @date 2021/2/25 0025 17:20
 */
public interface Constructable <T> {

    /**
     * 构造器
     * @return 类型
     */
    T construct();
}
