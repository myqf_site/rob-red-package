package com.landon.redpackage.module.red.service.impl;

import com.landon.redpackage.module.red.entity.RedRecordEntity;
import com.landon.redpackage.module.red.repository.RedRecordRepo;
import com.landon.redpackage.module.red.service.RedRecordService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author ：shishaopeng
 * @date ：Created in 2021/5/3 10:09 下午
 * @description：
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class RedRecordServiceImpl implements RedRecordService {

    private final RedRecordRepo redRecordRepo;

    public RedRecordServiceImpl(RedRecordRepo redRecordRepo) {
        this.redRecordRepo = redRecordRepo;
    }

    @Override
    public void save(RedRecordEntity redRecordEntity) {
        redRecordRepo.save(redRecordEntity);
    }
}
