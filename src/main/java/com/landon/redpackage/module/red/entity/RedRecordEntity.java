package com.landon.redpackage.module.red.entity;

import com.landon.redpackage.base.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import java.math.BigDecimal;

/**
 * @author ：shishaopeng
 * @date ：Created in 2021/5/3 3:58 下午
 * @description：发红包记录
 */
@Entity(name = "red_record")
@Data
@EqualsAndHashCode(callSuper = true)
public class RedRecordEntity extends BaseEntity {

    private Long userId;

    private String redPacket;

    private Integer total;

    private BigDecimal amount;

}
