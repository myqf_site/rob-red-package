package com.landon.redpackage.module.red.service.impl;

import com.landon.redpackage.module.red.entity.RedDetailEntity;
import com.landon.redpackage.module.red.repository.RedDetailRepo;
import com.landon.redpackage.module.red.service.RedDetailService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * @author ：shishaopeng
 * @date ：Created in 2021/5/3 10:08 下午
 * @description：
 */
@Service
@Transactional(rollbackOn = Exception.class)
public class RedDetailServiceImpl implements RedDetailService {

    private final RedDetailRepo redDetailRepo;

    public RedDetailServiceImpl(RedDetailRepo redDetailRepo) {
        this.redDetailRepo = redDetailRepo;
    }

    @Override
    public void save(RedDetailEntity redDetailEntity) {
        redDetailRepo.save(redDetailEntity);
    }
}
