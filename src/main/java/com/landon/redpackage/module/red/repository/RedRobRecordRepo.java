package com.landon.redpackage.module.red.repository;

import com.landon.redpackage.module.red.entity.RedRobRecordEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigDecimal;

/**
 * @author ：shishaopeng
 * @date ：Created in 2021/5/4 5:19 下午
 * @description：
 */
public interface RedRobRecordRepo extends JpaRepository<RedRobRecordEntity, Long> {

    /**
     * 根据用户id与红包字符串拿到该实体
     * @param userId 用户id
     * @param redPacket 全局唯一红包字符串
     * @param isActive 数据状态
     * @return RedRobRecordEntity
     */
    RedRobRecordEntity getByUserIdAndAndRedPacketAndIsActive(Integer userId, String redPacket, boolean isActive);

}
