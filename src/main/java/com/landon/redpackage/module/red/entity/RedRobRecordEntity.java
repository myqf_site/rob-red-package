package com.landon.redpackage.module.red.entity;

import com.landon.redpackage.base.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import java.math.BigDecimal;

/**
 * @author ：shishaopeng
 * @date ：Created in 2021/5/3 4:06 下午
 * @description：抢红包记录
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Entity(name = "red_rob_record")
public class RedRobRecordEntity extends BaseEntity {

    private Integer userId;

    private String redPacket;

    private BigDecimal amount;
}
