package com.landon.redpackage.module.red.service;

import com.landon.redpackage.module.red.entity.RedRobRecordEntity;


/**
 * @author ：shishaopeng
 * @date ：Created in 2021/5/4 5:19 下午
 * @description：
 */
public interface RedRobRecordService {

    /**
     * 根据用户id与红包字符串拿到该实体
     * @param userId 用户id
     * @param record 全部唯一字符串
     * @return RedRobRecordEntity
     */
    RedRobRecordEntity getByUserIdAndRedRecord(Integer userId, String record);

    /**
     * 抢红包信息入库
     * @param redRobRecordEntity 抢红包实体对象
     */
    void save(RedRobRecordEntity redRobRecordEntity);
}
