package com.landon.redpackage.module.red.service.impl;

import com.landon.redpackage.module.red.entity.RedRobRecordEntity;
import com.landon.redpackage.module.red.repository.RedRobRecordRepo;
import com.landon.redpackage.module.red.service.RedRobRecordService;
import org.springframework.stereotype.Service;


/**
 * @author ：shishaopeng
 * @date ：Created in 2021/5/4 5:20 下午
 * @description：
 */
@Service
public class RedRobRecordServiceImpl implements RedRobRecordService {

    private final RedRobRecordRepo redRobRecordRepo;

    public RedRobRecordServiceImpl(RedRobRecordRepo redRobRecordRepo) {
        this.redRobRecordRepo = redRobRecordRepo;
    }

    @Override
    public RedRobRecordEntity getByUserIdAndRedRecord(Integer userId, String record) {
        return redRobRecordRepo.getByUserIdAndAndRedPacketAndIsActive(userId, record, true);
    }

    @Override
    public void save(RedRobRecordEntity redRobRecordEntity) {
        redRobRecordRepo.save(redRobRecordEntity);
    }
}
