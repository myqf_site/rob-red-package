package com.landon.redpackage.module.red.service;

import com.landon.redpackage.module.red.entity.RedDetailEntity;

/**
 * @author ：shishaopeng
 * @date ：Created in 2021/5/3 10:08 下午
 * @description：
 */
public interface RedDetailService {

    /**
     * 保存红包
     * @param redDetailEntity 红包对象
     */
    void save(RedDetailEntity redDetailEntity);
}
