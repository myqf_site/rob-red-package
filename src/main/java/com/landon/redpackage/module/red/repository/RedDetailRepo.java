package com.landon.redpackage.module.red.repository;

import com.landon.redpackage.module.red.entity.RedDetailEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author ：shishaopeng
 * @date ：Created in 2021/5/3 10:06 下午
 * @description：
 */
public interface RedDetailRepo extends JpaRepository<RedDetailEntity, Long> {
}
