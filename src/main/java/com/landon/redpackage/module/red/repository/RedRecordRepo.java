package com.landon.redpackage.module.red.repository;

import com.landon.redpackage.module.red.entity.RedRecordEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author ：shishaopeng
 * @date ：Created in 2021/5/3 10:07 下午
 * @description：
 */
public interface RedRecordRepo extends JpaRepository<RedRecordEntity, Long> {
}
