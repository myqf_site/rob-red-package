package com.landon.redpackage.module.red.service;

import com.landon.redpackage.module.red.entity.RedRecordEntity;

/**
 * @author ：shishaopeng
 * @date ：Created in 2021/5/3 10:09 下午
 * @description：
 */
public interface RedRecordService {

    /**
     * 保存红包记录
     * @param redRecordEntity 红包记录对象
     */
    void save(RedRecordEntity redRecordEntity);
}
