package com.landon.redpackage.module.red.entity;

import com.landon.redpackage.base.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import java.math.BigDecimal;

/**
 * @author ：shishaopeng
 * @date ：Created in 2021/5/3 3:54 下午
 * @description：红包金额明细详情
 */
@EqualsAndHashCode(callSuper = true)
@Entity(name = "red_detail")
@Data
public class RedDetailEntity extends BaseEntity {

    private Long recordId;

    private BigDecimal amount;
}
