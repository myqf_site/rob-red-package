package com.landon.redpackage;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * @author shishaopeng
 */
@SpringBootApplication(scanBasePackages = {"com.landon.redpackage"})
@EntityScan(basePackages = {"com.landon.redpackage.**.entity"})
@EnableJpaRepositories(basePackages = {"com.landon.redpackage.**.repository"})
public class RedPackageApplication {

    public static void main(String[] args) {
        SpringApplication.run(RedPackageApplication.class, args);
    }

    /**
     * 多数据源源配置
     * @return HikariDataSource 资源
     */
    @Primary
    @Bean
    @ConfigurationProperties(prefix = "spring.datasource.hikari.datasource")
    public HikariDataSource dataSource() {
        return DataSourceBuilder.create()
                .type(HikariDataSource.class)
                .driverClassName("com.mysql.cj.jdbc.Driver")
                .url("jdbc:mysql://localhost:3306/red_package?serverTimezone=Asia/Shanghai&useUnicode=true&characterEncoding=utf-8&useSSL=false&allowPublicKeyRetrieval=true")
                .username("root")
                .password("123456")
                .build();
    }

}
