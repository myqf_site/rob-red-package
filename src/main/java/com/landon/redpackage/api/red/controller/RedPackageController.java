package com.landon.redpackage.api.red.controller;

import com.landon.redpackage.api.red.dto.RedPackageDto;
import com.landon.redpackage.api.red.facade.RedPackageFacade;
import com.landon.redpackage.base.dto.AppResponse;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

/**
 * @author ：shishaopeng
 * @date ：Created in 2021/5/3 5:19 下午
 * @description：
 */
@RestController
@RequestMapping("/red")
public class RedPackageController {


    private final RedPackageFacade redPackageFacade;

    public RedPackageController(RedPackageFacade redPackageFacade) {
        this.redPackageFacade = redPackageFacade;
    }


    @PostMapping("/hand-out")
    public AppResponse<String> handOutRedPackage(@RequestBody @Validated RedPackageDto redPackageDto) {
        return AppResponse.success(redPackageFacade.handOutRedPackage(redPackageDto));
    }

    @GetMapping("rob-red-package")
    public AppResponse<BigDecimal> robRedPackage(@RequestParam("newKey") String newKey, @RequestParam("userId") Integer userId) {
        return AppResponse.success(redPackageFacade.robRedPackage(newKey, userId));
    }
}
