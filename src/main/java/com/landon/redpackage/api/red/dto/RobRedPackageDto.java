package com.landon.redpackage.api.red.dto;

import com.sun.istack.NotNull;
import lombok.Data;

/**
 * @author ：shishaopeng
 * @date ：Created in 2021/5/4 5:04 下午
 * @description：抢红包实体对象
 */
@Data
public class RobRedPackageDto {

    private String newKey;

    @NotNull
    private Integer userId;
}
