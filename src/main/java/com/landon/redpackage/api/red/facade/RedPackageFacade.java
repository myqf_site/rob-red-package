package com.landon.redpackage.api.red.facade;

import com.landon.redpackage.api.red.dto.RedPackageDto;
import org.springframework.validation.annotation.Validated;

import java.math.BigDecimal;

/**
 * @author ：shishaopeng
 * @date ：Created in 2021/5/3 8:09 下午
 * @description：
 */
@Validated
public interface RedPackageFacade {

    /**
     * 发红包
     * @param redPackageDto 红包对象
     * @return 红包全局唯一标识
     */
    String handOutRedPackage(RedPackageDto redPackageDto);

    /**
     * 抢红包
     * @param newKey 红包全局唯一标识
     * @param userId 用户Id
     * @return 红包全局唯一标识
     */
    BigDecimal robRedPackage(String newKey, Integer userId);
}
