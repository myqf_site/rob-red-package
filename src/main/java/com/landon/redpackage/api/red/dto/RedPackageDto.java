package com.landon.redpackage.api.red.dto;

import com.sun.istack.NotNull;
import lombok.Data;


/**
 * @author ：shishaopeng
 * @date ：Created in 2021/5/3 8:05 下午
 * @description：
 */
@Data
public class RedPackageDto {

    /**
     * 用户id
     */
    private Long userId;
    /**
     * 红包数量
     */
    @NotNull
    private Integer total;
    /**
     * 总金额（分）
     */
    @NotNull
    private Integer amount;

}
