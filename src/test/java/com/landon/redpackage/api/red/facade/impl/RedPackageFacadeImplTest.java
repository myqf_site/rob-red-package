package com.landon.redpackage.api.red.facade.impl;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@SpringBootTest
class RedPackageFacadeImplTest {


    @Test
    void delverRedPackage_should_success_given_correct_list() {
        Integer amount = 10;
        Integer total = 10000;
        List<Integer> result = delverRedPackage(amount, total);
    }

    /**
     * 生成随机红包 以分为单位
     * @param amount 红包个数
     * @param total 总金额
     * @return 红包集合
     */
    private List<Integer> delverRedPackage(Integer amount, Integer total) {
        List<Integer> result = new ArrayList<>();
        boolean isNotNull = amount > 0 && total > 0;
        if (isNotNull) {
            int lastAmount = amount;
            int lastTotal = total;
            Random random = new Random();
            for (int i = 0; i < lastAmount-1; i++) {
                // 随机生成一个红包 [1, astTotal / lastAmount * 2 - 1）
                int redValue = random.nextInt(lastTotal / lastAmount * 2 - 1) + 1;
                result.add(redValue);
                lastAmount--;
                lastTotal -= redValue;
            }
            result.add(lastTotal);
        }
        return result;
    }
}