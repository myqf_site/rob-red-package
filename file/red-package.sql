create table red_detail (
    id int auto_increment primary key,
    record_id int not null comment '红包记录id',
    amount decimal(8,2) null comment '金额（单位为分）',
    is_active tinyint default 1 null,
    create_time datetime null
) comment '红包明细金额' charset=utf8;

create table red_record (
    id int auto_increment primary key,
    user_id int not null comment '用户id',
    red_packet varchar(255) charset utf8mb4 not null comment '红包全局唯一标识串',
    total int not null comment '人数',
    amount decimal(10,2) null comment '总金额（单位为分）',
    is_active tinyint default 1 null,
    create_time datetime null
) comment '发红包记录' charset=utf8;

create table red_rob_record (
    id int auto_increment primary key,
    user_id int null comment '用户账号',
    red_packet varchar(255) charset utf8mb4 null comment '红包标识串',
    amount decimal(8,2) null comment '红包金额（单位为分）',
    create_time datetime null comment '时间',
    is_active tinyint default 1 null
) comment '抢红包记录' charset=utf8;

