# 抢红包

#### 介绍
测试并发抢红包

#### 软件架构

软件架构与包体说明：

1. Controller     控制层
2. Facade         业务层
3. Service        服务调用层
4. Repo           数据库连接层

- api  
- base
- config
- moudle

MVCM中间件架构


#### 安装教程

1.  clone
2.  pull
3.  跑files里的SQL
4.  修改数据库连接

#### 使用说明

1.  启动应用，启动redis
2.  访问路径：http://localhost:8082/red-package-local/red/hand-out。
    请求参数放在body中：
                {
            	    "userId": 10025,
            	    "total": 10,
            	    "amount": 1000
                }
    响应实例：
                {
                    "success": true,
                    "message": "success",
                    "data": "redis:red:packet:10025:308469983966875",
                    "code": 20000
                }
3.  访问：http://localhost:8082/red-package-local/red/rob-red-package?newkey=XXX&userId=XXX
    还需要带上上一个路径返回的data：红包字符串 newKey
  
4. 启动Jmeter，新建测试计划，请求头信息设置、CSV用户ID文件上传（用Excel造几个用户id，另存为CSV文件）

5. 压测访问，debug代码...

#### 技术与工具选型

1.  SpringBoot
2.  data-redis
3.  data-jpa
4.  MySQL
5.  Jmeter

